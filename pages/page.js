import React, { Component } from 'react';
import Link from 'next/link';

export default class Page extends Component {
    render() {
        return (
            <div>
                <Link href='/'>
                    <a>Home</a>
                </Link>
                <h1>Navegamos para outra view</h1>
            </div>
        )
    }
}
